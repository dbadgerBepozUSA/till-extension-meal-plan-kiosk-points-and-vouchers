﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.CompilerServices;
using BepozCommon;
using BepozDLLBase;
using Xfer;
using Microsoft.VisualBasic.CompilerServices;
using System.IO;
using System.Threading.Tasks;

namespace TillX_MealPlanKiosk2
{
    public class clsMealPlanKiosk2 : clsBaseTillExtension
    {

        private string m_ExtensionName = "Autoscan Kiosk Till Extension";
        private int m_iProduct = 0;                     // product number for the meal plan
        private int m_iMealPlanDiscountGroup = 0;       // discount group to apply meal price reduction 
        private bool blDataScanned = false;             // flag set on when account data has been scanned at the beginning of a transaction 
        // if an account is opened with the flag on, process as a kiosk transaction
        private bool blAutoscanning = false;            // function key to enable autoscanning kiosk mode

        private cAccountFull oAccountFull = null;       // carry the account from data scan to rest of transaction
        private cAccountFull oParentGroupFull = null;   // carry the parent group data
        private int iPointsRequired = 0;                // when item is sold carry the points back to the account opened function
        private int iMealPlanNett = 0;                  // amount being discounted, carried from item sale to account opened
        private long lPointsAvailable = 0;              // points available, carried from account open to item sale to check points available before item sale
        private int iPointsRedeemed = 0;                // points redeemed, carried from item sale to account opened
        private string sLastAccountNumber = "";         // do not allow successive scans of the same accouont number

        private int iVoucherSetupId = 0;                // custom fields on the account or group determine the voucher being used
        private bool blUsePoints = false;               // custom fields on the account or group determine if points are used instead of vouchers
        private bool blRunVoucher = false;              // flag to trigger voucher payments after item sale
        private bool blAllow2xScan = false;             // flag to permit multiple scans of a card
        private bool m_blLogAll = false;                // flag to enable logging of scans
        private string sLogText = "";                   // string to accumulate the account progress after a scan
        private DateTime dtLastScan = DateTime.MinValue;    // replace the 2X scan flag with a datetime of last scan and time interval between successive uses of an account
        private int iMP2xScanMins = 0;                      // time interval between scans
        private int iAutoscanTransID = 0;               // transaction id for log file reporting

        // add ability to strip leading zeros or leave them as significant
        private bool m_blStripZeros = false;

        // disallow tender key when there is a balance due - this key is used for the "Settle Check" function to close out an open table
        private int m_iSettleCheck = 0;


        public override cDeviceSetup DeviceSettings(Int16 isSubtype)
        {
            var oDeviceSetup = new cDeviceSetup();
            oDeviceSetup.sName = m_ExtensionName;
            oDeviceSetup.blShowOptions = true;
            oDeviceSetup.sOptFlag1 = "Scan Mode On at Till Startup";
            oDeviceSetup.sOptFlag2 = "Log auto scans";
            oDeviceSetup.sOptFlag3 = "Remove Leading Zeros";
            oDeviceSetup.sOptNumber1 = "Product Number:";
            oDeviceSetup.sOptNumber2 = "Points Discount Totalizer (1-30):";
            oDeviceSetup.sOptNumber3 = "Settle Check Card Number (0-16):";
            oDeviceSetup.saToolTipFlag_[1] = "Autoscan defaults to on when checked, otherwise use function keys to toggle autoscan on and off";
            oDeviceSetup.saToolTipFlag_[2] = "Log automatic scans to Bepoz Data AutoscanLogs folder";
            oDeviceSetup.saToolTipFlag_[3] = "Ignore leading zeros in card number of account";
            oDeviceSetup.saToolTipNum_[1] = "Product number to be redeemed\r\nProduct selected must not require condiments";
            oDeviceSetup.saToolTipNum_[2] = "Venue Name discount number to apply redeem discount";
            oDeviceSetup.saToolTipNum_[3] = "Venue Credit Card Number for Settle Check function key\r\nDisallow this tender function key when the balance is not zero\r\nWhen set to zero this option is not used";
            return oDeviceSetup;
        }


        public override bool Initialise(cDeviceData oDeviceData, object oErrLogObject, CSettingsData oSettingsData)
        {
            base.Initialise(oDeviceData, oErrLogObject, oSettingsData);
            m_iProduct = oDeviceData.iaOptionNum_[1];
            m_iMealPlanDiscountGroup = oDeviceData.iaOptionNum_[2];
            m_iSettleCheck = oDeviceData.iaOptionNum_[3];
            if (oDeviceData.balOptionFlag_[1]) blAutoscanning = true;
            if (oDeviceData.balOptionFlag_[2]) m_blLogAll = true;
            if (oDeviceData.balOptionFlag_[3]) m_blStripZeros = true;
            // create the log files
            if (m_blLogAll)
            {
                string logText = "Starting SmartPOS";
                LogMPScanEvent(logText);
            }
            g_oTillFunctions.MessageShow(string.Format("Initialization...\r\n"+"{0} Initialized OK", m_ExtensionName), eMessageButtons.None);

            return true;
        }

        // if porting back to Bepoz 4.5.2  Function_ScanDataRaw(ref cCFButtonCopy oButton)

        public override bool Function_ScanDataRaw(ref cCFButtonCopy oButton, ref bool blScanDataBlocked)
        {
            if (blAutoscanning)
            {
                blDataScanned = true;
                sLogText = " Scan " + oButton.sKey + " ";

                // use the scan data to look up the account
                // Account_Lookup(string sAccNum, int iAccNum, int iAccountGroup, string sPrefix, bool blSetAsMobile, bool blDoNotAskCreate);
                // oAccountFull = g_oTillTransFuncs.Account_Lookup(oButton.sKey, 0, 0, null, true, false);

                //// this works, but does not use card number, it uses the account number
                ////oAccountFull = g_oTillTransFuncs.Account_Lookup(oButton.sKey, 0, 0, null, false, true);
                // the Account_Lookup will find the account and attach it to the transaction, but it can't use the card number and it can't be used for a parent

                // to look up by card number use the GetAccountbyNum
                //                                             .GetAccountbyNum(string sAccNum, string sCardNum, int iAccID)

                // update to allow or disallow the leading 0's of the card number
                string sLookupNumber = oButton.sKey;
                if (m_blStripZeros)
                {
                    sLookupNumber = oButton.sKey.TrimStart('0');
                }

                //oAccountFull = g_oTillTransFuncs.DatabaseObject.GetAccountbyNum("", oButton.sKey, 0);
                oAccountFull = g_oTillTransFuncs.DatabaseObject.GetAccountbyNum("", sLookupNumber, 0);


                // validate the accountLookup is an active account
                if (oAccountFull != null)
                {

                    // need to look at the account, its parent and determine whether we are to redeem points or voucher
                    // get the parent account
                    //                                             .GetAccountbyNum(string sAccNum, string sCardNum, int iAccID)
                    oParentGroupFull = g_oTillTransFuncs.DatabaseObject.GetAccountbyNum("", "", oAccountFull.Account.iParentID);

                    iVoucherSetupId = GetVoucherSetupId();
                    blUsePoints = GetPointsFlag();
                    blAllow2xScan = Get2XScanFlag();          // can the card be scanned twice in a row
                    iMP2xScanMins = GetAutoScanMins();        // number of minutes required before a second scan is allowed can override the flag to shut down second scan in a meal period
                    dtLastScan = GetLastAutoScan();           // the datetime of the last autoscan
                    m_iProduct = GetAutoScanProduct(m_iProduct);    // override the till device default product if there is on on the account parent group


                    if (oButton.sKey == sLastAccountNumber)
                    {
                        // this is a second scan of the same account
                        if (!blAllow2xScan)
                        {
                            // second scan of the same account is not allowed
                            sLogText = sLogText + " 2X scan error in group " + oAccountFull.Account.iParentID.ToString() + " ";
                            g_oTillFunctions.MessageShow("Multiple scans of the same account not permitted", eMessageButtons.OK);       // Error message
 
                            oButton = null;
                            blDataScanned = false;
                            LogMPScanEvent(sLogText);
                            return false;
                        }
                    }

                    if ((blAllow2xScan) && (dtLastScan.AddMinutes(iMP2xScanMins) > DateTime.Now))
                    {
                        // 2 scans are allowed but the the last scan + elapsed time is in the future (scan not allowed now)
                        sLogText = sLogText + " 2X scan time limit not met in group " + oAccountFull.Account.iParentID.ToString() + " ";
                        sLogText = sLogText + " Last scan " + dtLastScan.ToString("MMM dd HH:mm") + " Minimum time "+ iMP2xScanMins.ToString()+ " mins ";
                        g_oTillFunctions.MessageShow("Repeat scan of this account not allowed\r\nLast scan was "+dtLastScan.ToString("MMM dd HH:mm"), eMessageButtons.OK);       // Error message

                        oButton = null;
                        blDataScanned = false;
                        LogMPScanEvent(sLogText);
                        return false;
                    }

                    // this is not a second scan - or the second scan is allowed, either way ok to continue
                    sLastAccountNumber = oButton.sKey;      // save the last account number
                    UpdateLastAutoScan();                   // update the last scan datetime of the account


                    if (oAccountFull.Account.bStatus == 0)      // 0 = status active account
                    {
                        // use the account on this transaction
                        cCFButtonCopy oAccountSelectFunc = new cCFButtonCopy();
                        oAccountSelectFunc.iFunc = 1;           // account
                        oAccountSelectFunc.iSubFunc = 101;      // member charge
 
                        oAccountSelectFunc.sKey = oAccountFull.Account.sAccNumber;
                        sLogText = sLogText + " Account " + oAccountFull.Account.sAccNumber+" ";
                        g_oTillTransFuncs.Invoke_Function_Button(oAccountSelectFunc, true);
                        // this will trigger the AccountOpenedBefore and AccountOpened functions - set the sale and payment during AccountOpened

                        oButton = null;     // clear the scan data to prevent further processing
                        return false;
                    }
                }
                else
                {
                    // account is null
                    sLogText = sLogText + " Not an account ";
                }
                blDataScanned = false;

            }
            return base.Function_ScanDataRaw(ref oButton, ref blScanDataBlocked);      // if not using the data here, send it back
        }


        //public override bool Function_ScanDataDecoded(ref cHeaderCopy oHeaderCopy)
        //{
        //    return base.Function_ScanDataDecoded(ref oHeaderCopy);
        //}


        public override bool Function_Button(ref cCFButtonCopy obutton)
        {
            if ((obutton.iFunc == 23) && (obutton.iSubFunc == 9) && (obutton.iItem == 1) )       //  Custom Function, Till Extension, US Meal plan kiosk 
            {
                // till function button for Autoscan : Item = 1, Subitem not used, Key may be set to On or Off, otherwise button will toggle
                // toggle the autoscanning kiosk function on or off
                if (blAutoscanning)
                {
                    blAutoscanning = false;
                }
                else
                {
                    blAutoscanning = true;
                }
                if (obutton.sKey != null)
                {
                    if (obutton.sKey.ToUpper() == "ON") blAutoscanning = true;
                    if (obutton.sKey.ToUpper() == "OFF") blAutoscanning = false;
                }
                if (blAutoscanning)
                {
                    g_oTillFunctions.MessageShow("Autoscan enabled", eMessageButtons.OK);
                }
                else g_oTillFunctions.MessageShow("Autoscan disabled", eMessageButtons.OK);
            }

            // clear key pressed - going to believe that the autoscan item is being cleared and that the account holder is not wanting the planned product

            // check to see if the Settle Check function key is not allowed when a balance is due
            // the Settle Check function is a stand-beside type of credit (no authorization is automatically performed, it's up to the operator to run the card)
            // in the case of just needing to close out a check - we are using one of the card buttons to close tendered tables and checks when they have a zero balance
            // we don't want to allow that function key to close out a check that does have a balance
            if (m_iSettleCheck > 0)
            {
                if (obutton.iFunc == ModConstants.FUNC_TENDER)
                {
                    if (obutton.iSubFunc == ModConstants.SUBFUNC_TENDER_CCARD_BASE + m_iSettleCheck)  // 512 = base + credit card number selected  cc1 = 512, cc16 = 528
                    {
                        // settle check function and a balance due is not permitted
                        cTillTransaction oCurrentTillTransaction = g_oTillTransFuncs.Trans_Current();
                        double dCurrentBalanceDue = g_oTillTransFuncs.Trans_Balance(oCurrentTillTransaction);
                        if (dCurrentBalanceDue != 0d)
                        {
                            g_oTillFunctions.MessageShow(obutton.sText + " not permitted when a balance is due", eMessageButtons.OK);
                            obutton = new cCFButtonCopy();
                            return true;        // return false allows processing to continue
                        }
                    }
                }
            }

            return base.Function_Button(ref obutton);
        }


        public override void AccountOpenBefore(ref cAccountFull oAccountFull)
        {
            base.AccountOpenBefore(ref oAccountFull);
        }


        public override void AccountOpened(ref cAccountFull oAccountFull)
        {
            if (blAutoscanning)
            {
                if (blDataScanned)
                {

                    cTillTransaction oTillTrans = null;
                    oTillTrans = g_oTillTransFuncs.Trans_Current();
                    iAutoscanTransID = oTillTrans.Orderline.@base.Saved.iTransactionID;
                    sLogText = sLogText + " Transaction " + iAutoscanTransID.ToString() + " ";

                    // calculate the points available
                    lPointsAvailable = oAccountFull.Account.lPointsEarned - oAccountFull.Account.lPointsRedeemed;

                    // figure out what item to sell - this is set for the till device or for the account parent group
                    // can we get the shift?
                    // can we sell a different item for each shift?

                    bool blFunctionEnd = true;
                    // use a function button to sell the item:
                    cCFButtonCopy oMealItemFunc = new cCFButtonCopy();
                    oMealItemFunc.iFunc = 13;           // PLU
                    oMealItemFunc.iSubFunc = 1;         // PLU-fixed
                    oMealItemFunc.iItem = m_iProduct;

                    ////  check the product before attempting the sale? - product doesn't know about the promo (points or voucher) 
                    ////cProductFull oProductFull = new cProductFull();
                    ////oProductFull = g_oTillTransFuncs.DatabaseObject.GetProductFull(m_iProduct);
                    sLogText = sLogText + " Product " + m_iProduct.ToString();
                    g_oTillTransFuncs.Invoke_Function_Button(oMealItemFunc, blFunctionEnd);


                    // if points were not redeemed skip the order line and payment line
                    if (iPointsRedeemed > 0)
                    {
                        // group controls account points vs voucher
                        int iGroupAccountID = oAccountFull.iBaseAccountGroup;


                        // order record includes discount info
                        cTILLORDERDATA cTILLORDERDATAum = new cTILLORDERDATA();
                        cTILLORDERDATA cTILLORDERDATAum1 = cTILLORDERDATAum;
                        cTransactions savedorder = cTILLORDERDATAum1.@base.Saved;
                        savedorder.iAccountID = oAccountFull.Account.iAccountID;
                        savedorder.iDiscAmtAmt = iMealPlanNett * (-1);
                        savedorder.iDiscAmtID = m_iMealPlanDiscountGroup;

                        // the points required for this item are calculated during the item sale as iPointsRequired
                        // redeem points - base Bepoz will use the sale item points to calculate the points redeemed

                        // payment of 0 cash to close the transaction
                        cTILLPAYMENTDATA cTILLPAYMENTDATAum = new cTILLPAYMENTDATA();
                        cTILLPAYMENTDATA cTILLPAYMENTDATAum1 = cTILLPAYMENTDATAum;
                        cTransPayments savedpmt = cTILLPAYMENTDATAum1.@base.Saved;
                        // this should be a cash tender to close out the transaction paymentType = 1
                        savedpmt.iAdjustment = 0;
                        savedpmt.iAmount = 0;
                        savedpmt.iPaymentType = 1;
                        ////savedpmt.iPaymentType = 18;     // payment type 18 = pointscashed
                        ////                                // payment type 16 = acccharge
                        savedpmt.iaTPayFlag[1] = 2048;
                        g_oTillTransFuncs.Payment_Add(cTILLPAYMENTDATAum, false);
                    }
                    if (blRunVoucher)
                    {
                        // need to determine if voucher ran and the transaction can be closed
                        //bool okToClose = false;

                        cTillTransaction oTillTransaction = new cTillTransaction();
                        oTillTransaction = g_oTillTransFuncs.Trans_Current();
                        double balance = g_oTillTransFuncs.Trans_Balance(oTillTransaction);
                        if (balance == 0)
                        {
                            // payment of 0 cash to close the transaction
                            cTILLPAYMENTDATA cTILLPAYMENTDATAum = new cTILLPAYMENTDATA();
                            cTILLPAYMENTDATA cTILLPAYMENTDATAum1 = cTILLPAYMENTDATAum;
                            cTransPayments savedpmt = cTILLPAYMENTDATAum1.@base.Saved;
                            // this should be a cash tender to close out the transaction paymentType = 1
                            savedpmt.iAdjustment = 0;
                            savedpmt.iAmount = 0;
                            savedpmt.iPaymentType = 1;
                            ////savedpmt.iPaymentType = 18;     // payment type 18 = pointscashed
                            ////                                // payment type 16 = acccharge
                            savedpmt.iaTPayFlag[1] = 2048;
                            g_oTillTransFuncs.Payment_Add(cTILLPAYMENTDATAum, false);
                        }
                    }
                }
            }
            base.AccountOpened(ref oAccountFull);
        }


        public override bool Sale_Add_Before(ref cTILLSALEDATA oSalesitem, ref bool blSuppressDisplay)
        {
            if (blAutoscanning)
            {
                if (blDataScanned)
                {
                    if (!oAccountFull.Account.blPointsRedeem && blUsePoints)        // account not allowed to redeem points and group is set to redeem points
                    {
                        // not allowed to redeem points on this account
                        // keep the account, but exit the autoscan
                        blDataScanned = false;
                        sLogText = sLogText + " Account not set for points redeem - exit autoscan transaction";
                        LogMPScanEvent(sLogText);
                        return true;
                    }
                    iPointsRequired = oSalesitem.@base.iPointsReqdEach;     // set the points required for the item

                    if (blUsePoints)
                    {

                        int whatToDo = 0;
                        if (iPointsRequired > lPointsAvailable)                 // insufficient points 
                        {
                            // not going to allow the redeemption
                            sLogText = sLogText + " Insufficient points " + lPointsAvailable.ToString()+"/"+iPointsRequired.ToString()+" ";
                            whatToDo = g_oTillFunctions.MessageShow("Insufficient points for this item, purchase item?", eMessageButtons.YesNoCancel);
                            // yes = 6
                            // no = 7
                            // cancel = 2

                            if (whatToDo == 7)      // no, keep the account, but do not add this item
                            {
                                blDataScanned = false;
                                sLogText = sLogText + " Insufficient points Product not sold - exit autoscan transaction";
                                LogMPScanEvent(sLogText);
                                return true;
                            }
                            if (whatToDo == 6)      // yes, keep the account, add the item, but do not try to pay with points
                            {
                                blDataScanned = false;
                                sLogText = sLogText + " Insufficient points Product sold - exit autoscan transaction";
                                LogMPScanEvent(sLogText);
                                return base.Sale_Add_Before(ref oSalesitem, ref blSuppressDisplay);
                            }
                            if (whatToDo == 2)      // cancel, abandon the sale, remove the account
                            {
                                blDataScanned = false;
                                cCFButtonCopy oClearCancelFunc = new cCFButtonCopy();
                                oClearCancelFunc.iFunc = 3;
                                oClearCancelFunc.iSubFunc = 2;
                                g_oTillTransFuncs.Invoke_Function_Button(oClearCancelFunc, true);
                                sLogText = sLogText + " Insufficient points Transaction cancelled - exit autoscan transaction";
                                LogMPScanEvent(sLogText);
                                return true;
                            }
                        }
                        // continue with item purchase using points redemption
                        oSalesitem.@base.Saved.dbUnitqtyRedeem = 1;
                        oSalesitem.@base.Saved.iPointsRedeem = iPointsRequired;
                        iMealPlanNett = oSalesitem.@base.Saved.iNett;
                        // gross amount and the discount number
                        byte baDiscountGroup = (byte)m_iMealPlanDiscountGroup;
                        oSalesitem.@base.Saved.baDiscNum_[2] = (byte)m_iMealPlanDiscountGroup;
                        oSalesitem.@base.Saved.iaDiscAmt_[2] = iMealPlanNett * (-1);
                        oSalesitem.@base.Saved.iGross = iMealPlanNett;
                        // reset the line item nett to zero, reflecting paid for with points
                        oSalesitem.@base.Saved.iNett = 0;
                        iPointsRedeemed = iPointsRequired;
                        sLogText = sLogText + " Redeemed points " + (iPointsRedeemed/100).ToString() + " ";
                    }
                    else
                    {
                        // set flag for voucher redemption after sale item
                        blRunVoucher = true;
                    }

                }
                blDataScanned = false;                                  // a sale is being performed, set off the data scanned flag - don't process as a kiosk
            }
            return base.Sale_Add_Before(ref oSalesitem, ref blSuppressDisplay);
        }

        // Bepoz 4.7:
        public override bool Sale_Add_After(ref cTILLSALEDATA oSalesitem, int iInsertAfter_, ref cTillTransaction oTillTrans)
        {

            if (blAutoscanning)
            {

                if (blRunVoucher)
                {
                    // need to check for multiple voucher of the same setup id

                    // step through the vouchers on oAccountFull.VoucherIDbyProdPromo[].iField5 and see how many match the setup id
                    // if only one - we can call it just with the setup id in iSubItem
                    // if multiple need to decide which one to use and assign it to iItem
                    // I see no harm in assigning the voucher regardless of the count at this time
                    int voucherIdToUse = 0;
                    int voucherSetup = 0;

                    bool blWhatUsedToWork = false;      // there was a list of vouchers on the fullaccount - finding that not populated - need to look up the vouchers for the account
                    if (!blWhatUsedToWork)
                    {
                        cVoucherFull[] oVouchers = g_oTillTransFuncs.DatabaseObject.GetVouchers(0, 0, 0, oAccountFull.Account.iAccountID);
                        for (int i = 0; i < oVouchers.Length; i++)
                        {
                            if (oVouchers[i].VoucherSetup.iVoucherSetupID == iVoucherSetupId)
                            {
                                if (oVouchers[i].Voucher.bStopRedeems == 0)     // stop redeems is off
                                {
                                    if (VoucherDateOK(oVouchers[i]))            // voucher started and not expired
                                    {
                                        if (oVouchers[i].Voucher.bUnlimitedUse == 1)     // unlimited use is on
                                        {
                                            voucherIdToUse = oVouchers[i].Voucher.iVoucherID;
                                        }
                                        else
                                        {
                                            // do we need to check the amount left against the cost - or does this depend on the voucher type?  points left is greater than or equal to the points required
                                            if (!(oVouchers[i].Voucher.iAmountLeft < iPointsRequired))
                                            {
                                                voucherIdToUse = oVouchers[i].Voucher.iVoucherID;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // finding cases where account has vouchers but they are not listed in VoucherIDsbyProdPromo[]
                        if (oAccountFull.VoucherIDsbyProdPromo == null)
                        {
                            sLogText = sLogText + "/" + " Account record does not contain voucher ids.";
                        }
                        else
                        {
                            sLogText = sLogText + "/" + " Account record contains " + oAccountFull.VoucherIDsbyProdPromo.Length.ToString() + " vouchers.";
                            for (int i = 0; i < oAccountFull.VoucherIDsbyProdPromo.Length; i++)
                            {
                                Int32.TryParse(oAccountFull.VoucherIDsbyProdPromo[i].sField5, out voucherSetup);
                                if (voucherSetup == iVoucherSetupId)  //need to match the voucher Setup Id
                                {
                                    // for each voucher setup that is a match, use the voucher id found
                                    // the vouchers are newest at index 0 and older as the list is processed
                                    // if we wanted to use the newest voucher instead of the oldest, we should exit the for loop when the first one is found
                                    // by setting the LCV to end of the array:  i = oAccountFull.VoucherIDsbyProdPromo.Length;
                                    voucherIdToUse = oAccountFull.VoucherIDsbyProdPromo[i].iField1;     // the last one on the list is the oldest
                                    sLogText = sLogText + "/" + " Voucher ID of oldest matching voucher =" + voucherIdToUse + ".";
                                }
                            }
                        }
                    }


                    if (!(voucherIdToUse == 0))
                    {
                        // set up the invoke function button to redeem voucher
                        cCFButtonCopy oVoucherRedeemFunc = new cCFButtonCopy();
                        oVoucherRedeemFunc.iFunc = 7;           // discount
                        oVoucherRedeemFunc.iSubFunc = 22;       // redeem voucher
                        oVoucherRedeemFunc.iSubItem = iVoucherSetupId;      // subitem voucher setup id
                        oVoucherRedeemFunc.iItem = voucherIdToUse;          // this is required if there are more than one vouchers otherwise Bepoz will prompt which to use
                        g_oTillTransFuncs.Invoke_Function_Button(oVoucherRedeemFunc, true);
                        sLogText = sLogText + "/" + " Voucher Setup Id " + iVoucherSetupId.ToString() + " Voucher Id Used " + voucherIdToUse.ToString() + " sent with Discount Redeem Voucher function key.";

                        cTillTransaction oTillTransaction = new cTillTransaction();
                        oTillTransaction = g_oTillTransFuncs.Trans_Current();
                        //// look at sale line item for voucher id that was used then log it
                        // extensive logging already done
                        //if (oTillTransaction.SaleLines != null) // getting voucher reference number in paymentaddafter
                        //{
                        //    int index = oSalesitem.iIndex - 1;
                        //    if (index < 0) index = 0;
                        //    sLogText = sLogText + "/" + oTillTransaction.SaleLines[index].@base.iVoucherID.ToString() + " ";
                        //}
                    }
                }
            }

            return base.Sale_Add_After(ref oSalesitem, iInsertAfter_, ref oTillTrans);
        }


        // Bepoz 4.6:
        ////public override bool Sale_Add_After(ref cTILLSALEDATA oSalesitem, int iInsertAfter_)
        ////{
        ////    if (blAutoscanning)
        ////    {

        ////        if (blRunVoucher)
        ////        {
        ////            // need to check for multiple voucher of the same setup id

        ////            // step through the vouchers on oAccountFull.VoucherIDbyProdPromo[].iField5 and see how many match the setup id
        ////            // if only one - we can call it just with the setup id in iSubItem
        ////            // if multiple need to decide which one to use and assign it to iItem
        ////            // I see no harm in assigning the voucher regardless of the count at this time
        ////            int voucherIdToUse = 0;
        ////            int voucherSetup = 0;

        ////            bool blWhatUsedToWork = false;      // there was a list of vouchers on the fullaccount - finding that not populated - need to look up the vouchers for the account
        ////            if (!blWhatUsedToWork)
        ////            {
        ////                cVoucherFull[] oVouchers = g_oTillTransFuncs.DatabaseObject.GetVouchers(0, 0, 0, oAccountFull.Account.iAccountID);
        ////                for (int i = 0; i < oVouchers.Length; i++)
        ////                {
        ////                    if (oVouchers[i].VoucherSetup.iVoucherSetupID == iVoucherSetupId)
        ////                    {
        ////                        if (oVouchers[i].Voucher.bStopRedeems == 0)     // stop redeems is off
        ////                        {
        ////                            if (VoucherDateOK(oVouchers[i]))            // voucher started and not expired
        ////                            {
        ////                                if (oVouchers[i].Voucher.bUnlimitedUse == 1)     // unlimited use is on
        ////                                {
        ////                                    voucherIdToUse = oVouchers[i].Voucher.iVoucherID;
        ////                                }
        ////                                else
        ////                                {
        ////                                    // do we need to check the amount left against the cost - or does this depend on the voucher type?  points left is greater than or equal to the points required
        ////                                    if (!(oVouchers[i].Voucher.iAmountLeft < iPointsRequired))
        ////                                    {
        ////                                        voucherIdToUse = oVouchers[i].Voucher.iVoucherID;
        ////                                    }
        ////                                }
        ////                            }
        ////                        }
        ////                    }
        ////                }
        ////            }
        ////            else
        ////            {
        ////                // finding cases where account has vouchers but they are not listed in VoucherIDsbyProdPromo[]
        ////                if (oAccountFull.VoucherIDsbyProdPromo == null)
        ////                {
        ////                    sLogText = sLogText + "/" + " Account record does not contain voucher ids.";
        ////                }
        ////                else
        ////                {
        ////                    sLogText = sLogText + "/" + " Account record contains " + oAccountFull.VoucherIDsbyProdPromo.Length.ToString() + " vouchers.";
        ////                    for (int i = 0; i < oAccountFull.VoucherIDsbyProdPromo.Length; i++)
        ////                    {
        ////                        Int32.TryParse(oAccountFull.VoucherIDsbyProdPromo[i].sField5, out voucherSetup);
        ////                        if (voucherSetup == iVoucherSetupId)  //need to match the voucher Setup Id
        ////                        {
        ////                            // for each voucher setup that is a match, use the voucher id found
        ////                            // the vouchers are newest at index 0 and older as the list is processed
        ////                            // if we wanted to use the newest voucher instead of the oldest, we should exit the for loop when the first one is found
        ////                            // by setting the LCV to end of the array:  i = oAccountFull.VoucherIDsbyProdPromo.Length;
        ////                            voucherIdToUse = oAccountFull.VoucherIDsbyProdPromo[i].iField1;     // the last one on the list is the oldest
        ////                            sLogText = sLogText + "/" + " Voucher ID of oldest matching voucher =" + voucherIdToUse + ".";
        ////                        }
        ////                    }
        ////                }
        ////            }


        ////            if (!(voucherIdToUse == 0))
        ////            {
        ////                // set up the invoke function button to redeem voucher
        ////                cCFButtonCopy oVoucherRedeemFunc = new cCFButtonCopy();
        ////                oVoucherRedeemFunc.iFunc = 7;           // discount
        ////                oVoucherRedeemFunc.iSubFunc = 22;       // redeem voucher
        ////                oVoucherRedeemFunc.iSubItem = iVoucherSetupId;      // subitem voucher setup id
        ////                oVoucherRedeemFunc.iItem = voucherIdToUse;          // this is required if there are more than one vouchers otherwise Bepoz will prompt which to use
        ////                g_oTillTransFuncs.Invoke_Function_Button(oVoucherRedeemFunc, true);
        ////                sLogText = sLogText + "/" + " Voucher Setup Id " + iVoucherSetupId.ToString() + " Voucher Id Used " + voucherIdToUse.ToString() + " sent with Discount Redeem Voucher function key.";

        ////                cTillTransaction oTillTransaction = new cTillTransaction();
        ////                oTillTransaction = g_oTillTransFuncs.Trans_Current();
        ////                //// look at sale line item for voucher id that was used then log it
        ////                // extensive logging already done
        ////                //if (oTillTransaction.SaleLines != null) // getting voucher reference number in paymentaddafter
        ////                //{
        ////                //    int index = oSalesitem.iIndex - 1;
        ////                //    if (index < 0) index = 0;
        ////                //    sLogText = sLogText + "/" + oTillTransaction.SaleLines[index].@base.iVoucherID.ToString() + " ";
        ////                //}
        ////            }
        ////        }
        ////    }
        ////    return base.Sale_Add_After(ref oSalesitem, iInsertAfter_);
        ////}


        public override bool SalesLine_SetPoints(ref cTILLSALEDATA oSalesline, ref cPriceList.uProdPrice oProdPrice, ref bool blChanged)
        {
            return base.SalesLine_SetPoints(ref oSalesline, ref oProdPrice, ref blChanged);
        }


        public override bool Payment_Add_Before(ref cTILLPAYMENTDATA uPaymentitem, ref bool blDonotFinalise)
        {
            return base.Payment_Add_Before(ref uPaymentitem, ref blDonotFinalise);
        }


        public override bool Payment_Add_After(ref cTILLPAYMENTDATA uPaymentitem)
        {
            if (blAutoscanning)
            {
                sLogText = sLogText + " Payment" + " $" + (uPaymentitem.@base.Saved.iAmount/100).ToString();
                if (blRunVoucher)
                {
                    sLogText = sLogText + " Voucher " + uPaymentitem.@base.Saved.sRefText;
                }
            }
            return base.Payment_Add_After(ref uPaymentitem);
        }


        public override List<cProdPromoFull> AccountPromos(ref cAccount oAccount)
        {
            return base.AccountPromos(ref oAccount);
        }


        //public override void SetGlobals(cGlobal oGlobal, cVenue oVenue, cStore ostore, cWorkstation oWorkstation, cExternalPayments[] oVenueExtPay)
        //{
        //    base.SetGlobals(oGlobal, oVenue, ostore, oWorkstation, oVenueExtPay);
        //}


        public override void Function_End_Trans()
        {
            if (blAutoscanning)
            {
                sLogText = sLogText + " End Transaction " + iAutoscanTransID.ToString();
                LogMPScanEvent(sLogText);
                oAccountFull = null;
                oParentGroupFull = null;
                iPointsRequired = 0;
                iPointsRedeemed = 0;
                lPointsAvailable = 0;
                blRunVoucher = false;
                sLogText = "";
            }
            base.Function_End_Trans();
        }


        private int GetVoucherSetupId()
        {
            // custom fields are defined for vouchers if they are being used
            // get the custom field index - get all custom fields of the same type and then search on the name
            // note that the custom fields are indexed by 1 so that our first 
            // also note that the database value is stored with 2 implied decimal points
            int setupId = 0;
            cCustomField[] oCustomFieldNumbers = new cCustomField[30];
            // Bepoz 4.6:  oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Int32);
            oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Number);       // Bepoz 4.7
            for (int i = 0; i < oCustomFieldNumbers.Length; i++)
            {
                string nametest = oCustomFieldNumbers[i].sName.ToUpper();
                if (nametest.Contains("VOUCHER SETUP ID"))
                {
                    // look up the voucher id on the account and parent
                    //setupId = oAccountFull.Account.iaCustomNum_[i + 1]/100;
                    //if (setupId == 0)
                    //{
                    // voucher id is set on the parent group only - do not override the group setting with an account setting
                    setupId = oParentGroupFull.Account.iaCustomNum_[i + 1]/100;
                    // when found, exit the loop
                    i = oCustomFieldNumbers.Length;
                    //}
                }
            }
            sLogText = sLogText + " Voucher setup id " + setupId.ToString() + " ";
            return setupId;
        }

        private bool GetPointsFlag()
        {
            bool flag = false;
            cCustomField[] oCustomFieldFlags = new cCustomField[30];
            oCustomFieldFlags = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Flag);
            for (int i = 0; i < oCustomFieldFlags.Length; i++)
            {
                string nametest = oCustomFieldFlags[i].sName.ToUpper();
                if (nametest.Contains("USE POINTS"))
                {
                    //flag = oAccountFull.Account.balCustomFlag_[i + 1];
                    //if (flag == false)
                    //{
                        // points flag is set on the parent group only
                        flag = oParentGroupFull.Account.balCustomFlag_[i + 1];
                    // when found, exit the loop
                    i = oCustomFieldFlags.Length;                    //}
                }
            }
            sLogText = sLogText + " Points flag " + flag.ToString() + " ";
            return flag;
        }

        private bool Get2XScanFlag()
        {
            bool flag = false;
            cCustomField[] oCustomFieldFlags = new cCustomField[30];
            oCustomFieldFlags = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Flag);
            for (int i = 0; i < oCustomFieldFlags.Length; i++)
            {
                string nametest = oCustomFieldFlags[i].sName.ToUpper();
                if (nametest.Contains("ALLOW 2X SCAN"))
                {
                    // flag is set on parent group
                    flag = oParentGroupFull.Account.balCustomFlag_[i + 1];
                    // when found, exit the loop
                    i = oCustomFieldFlags.Length;
                }
            }
            return flag;
        }



        private DateTime GetLastAutoScan()
        {
            DateTime dttm = DateTime.MinValue;
            cCustomField[] oCustomFieldDateTimes = new cCustomField[30];
            oCustomFieldDateTimes = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Date);
            for (int i = 0; i < oCustomFieldDateTimes.Length; i++)
            {
                string nametest = oCustomFieldDateTimes[i].sName.ToUpper();
                if (nametest.Contains("LAST AUTOSCAN"))
                {
                    // look up the last scan date time on the account
                    dttm = oAccountFull.Account.dtaCustomDate_[i + 1];
                    i = oCustomFieldDateTimes.Length;
                }
            }
            return dttm;
        }

        private int GetAutoScanMins()
        {
            // minutes required between meal plan scan of same account
            int scanmins = 0;
            cCustomField[] oCustomFieldNumbers = new cCustomField[30];
            // Bepoz 4.6:  oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Int32);
            oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Number);        // Bepoz 4.7
            for (int i = 0; i < oCustomFieldNumbers.Length; i++)
            {
                string nametest = oCustomFieldNumbers[i].sName.ToUpper();
                if (nametest.Contains("2X SCAN MIN"))
                {
                    // use the parent only
                    scanmins = oParentGroupFull.Account.iaCustomNum_[i + 1] / 100;
                    // when found, exit the loop
                    i = oCustomFieldNumbers.Length;
                }
            }
            return scanmins;
        }


        private int GetAutoScanProduct(int iAutoProd)
        {
            // override the till device meal product with account group custom product
            cCustomField[] oCustomFieldNumbers = new cCustomField[30];
            // Bepoz 4.6:  oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Int32);
            oCustomFieldNumbers = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Number);       // Bepoz 4.7
            for (int i = 0; i < oCustomFieldNumbers.Length; i++)
            {
                string nametest = oCustomFieldNumbers[i].sName.ToUpper();
                if (nametest.Contains("AUTOSCAN PRODUCT"))
                {
                    // use the parent IF it is not 0
                    int tempAutoProd = oParentGroupFull.Account.iaCustomNum_[i + 1] / 100;
                    if (tempAutoProd > 0) iAutoProd = tempAutoProd;
                    // when found, exit the loop
                    i = oCustomFieldNumbers.Length;
                }
            }
            return iAutoProd;
        }


        private void UpdateLastAutoScan()
        {
            DateTime dttm = DateTime.Now;
            cCustomField[] oCustomFieldDateTimes = new cCustomField[30];
            oCustomFieldDateTimes = g_oTillTransFuncs.DatabaseObject.GetCustomFields(eCustomTableID.Accounts, eCustomFieldType.Date);
            for (int i = 0; i < oCustomFieldDateTimes.Length; i++)
            {
                string nametest = oCustomFieldDateTimes[i].sName.ToUpper();
                if (nametest.Contains("LAST AUTOSCAN"))
                {
                    // set the last scan date time on the account
                    // SetAccountCustom(int iAccountID, 
                    // datetimes are custom field indexes 201 - 205
                    int iCustomField = 201 + i;
                    g_oTillTransFuncs.DatabaseObject.SetAccountCustom(oAccountFull.Account.iAccountID, iCustomField, DateTime.Now.ToString());
                    // when found, exit the loop
                    i = oCustomFieldDateTimes.Length;
                }
            }
            return;
        }


        private bool VoucherDateOK(cVoucherFull oVoucher)
        {
            bool blOK = false;
            DateTime dtNow = DateTime.Now;
            DateTime dtVoucherStart = oVoucher.Voucher.dtDateStart;
            DateTime dtVoucherEnd = oVoucher.Voucher.dtDateExpiry;

            //// Voucher setup fields - not checking these - assuming that base calculates these and posts valid start/end times to the cVoucherFull.Voucher.dt
            ////  this doesn't seem to apply when the setup has been changed
            //// Expiry Type  - exact date, none, number of days, hours, minutes, months, weeks, years, today only
            //byte bVoucherSetupExpType = oVoucher.VoucherSetup.bExpiryType;
            //// number used with type days, hours, minutes, months, weeks, years
            //int iVoucherSetupDWMY = oVoucher.VoucherSetup.isExpiryNumber;

            if ((dtNow >= dtVoucherStart) && (dtNow <= dtVoucherEnd))
            {
                blOK = true;
            }
            blOK = true;
            return blOK;
        }


        private void LogMPScanEvent(string eventText)
        {
            if (m_blLogAll)
            {
                bool append = true;

                string eventLogDateTime = DateTime.Now.ToString("yyyyMMdd HH:mm.fff");
                string logMsg = eventLogDateTime + " - " + eventText + "\r\n";

                // just try to write to today's file, if it fails, create it and write to it
                string eventLogDate = DateTime.Now.ToString("yyyyMMdd");
                string todaysFileName = eventLogDate + "Autoscan.log";
                string dataPath = g_osettings.sLocalDataPath;
                string subDir = "AutoscanLogs";
                string logPath = Path.Combine(dataPath, subDir);
                DirectoryInfo di = Directory.CreateDirectory(logPath);          // create the subdirectory if it does not exist
                string verificationLogPathFileName = Path.Combine(logPath, todaysFileName);
                if (File.Exists(verificationLogPathFileName))
                {
                    append = true;
                }
                else
                {
                    append = false;
                    logMsg = eventLogDateTime + " - Autoscan Log File for " + DateTime.Today.ToString("dd MMM yyyy") + "\r\n" + logMsg;
                    // check for expired files
                    //// clean up older than 7 day logs
                    string[] files = Directory.GetFiles(logPath);
                    foreach (string file in files)
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.Name.Contains("MPScan.log"))
                        {
                            // if (fi.LastAccessTime < DateTime.Now.AddMonths(-3))  // last accessed 90 days ago
                            if (fi.LastWriteTime < DateTime.Now.AddDays(-14))  // last written 14 days ago
                                fi.Delete();
                        }
                    }
                }
                using (var sw = new StreamWriter(verificationLogPathFileName, append, Encoding.Default))
                {
                    sw.Write(logMsg);
                    sw.Close();
                }
            }
        }

    }
}
